﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MenuPizza.Views
{ 
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuPage : ContentPage
	{
        int c = 3, m = 5,cc = 6, g = 8,a = 10, f = 12, b = 120, t = 30;
        int c2, t2, total, multi;
        bool peperoni, chile, queso, cebolla, aceituna, carne, champo;
        public MenuPage ()
		{
			InitializeComponent ();
            pizzaPequenia.BackgroundColor = Color.BurlyWood;
        }
        void CalcularPrecio(object sender, EventArgs args)
        {
            Evaluar();
            total = b + t2 + c2;
            precio.Text = "Total: " + total;
        }
        void Refrescar()
        {
            peperoni = false;
            chile = false;
            cebolla = false;
            aceituna = false;
            champo = false;
            queso = false;
            carne = false;

        }
        void Evaluar()
        {
            int cpeperoni, cchile, cqueso, ccebolla, caceituna, ccarne, cchampo;
            if (peperoni==true)
            {cpeperoni = (a* multi);
            }else{
                cpeperoni = 0;}
            if (chile==true)
            {cchile = (cc * multi);
            }else{
                cchile = 0;}
            if (queso == true)
            {cqueso = (f * multi);
            }else{
                cqueso = 0;}
            if (cebolla == true)
            {ccebolla = (f * multi);
            }else{
                ccebolla = 0;}
            if (aceituna == true)
            {caceituna = (m * multi);
            }else{
                caceituna = 0;}
            if (carne == true)
            {ccarne = 8 + (f * multi);
            }else{
                ccarne = 0;}
            if (champo == true)
            {cchampo = (g * multi);
            }else{
                cchampo = 0;}
            c2 = cpeperoni + cchile + cqueso + ccebolla + caceituna + ccarne + cchampo;
        }

        void PequeniaPrecioCommand(object sender, EventArgs args)
        {
            multi = 1;
            pizzaPequenia.BackgroundColor = Color.BurlyWood;
            pizzaMediana.BackgroundColor = Color.Default;
            pizzaGrande.BackgroundColor = Color.Default;
            pizzaFamiliar.BackgroundColor = Color.Default;
            t2 = (multi*t)-30;
        }

        void MedianaPrecioCommand(object sender, EventArgs args)
        {
            multi = 2;
            pizzaPequenia.BackgroundColor = Color.Default;
            pizzaMediana.BackgroundColor = Color.BurlyWood;
            pizzaGrande.BackgroundColor = Color.Default;
            pizzaFamiliar.BackgroundColor = Color.Default;
            t2 = (multi * t) - 30;
        }

        void GrandePrecioCommand(object sender, EventArgs args)
        {
            multi = 3;
            pizzaPequenia.BackgroundColor = Color.Default;
            pizzaMediana.BackgroundColor = Color.Default;
            pizzaGrande.BackgroundColor = Color.BurlyWood;
            pizzaFamiliar.BackgroundColor = Color.Default;
            t2 = (multi * t) - 30;
        }

        void FamaliarPrecioCommand(object sender, EventArgs args)
        {
            multi = 4;
            pizzaPequenia.BackgroundColor = Color.Default;
            pizzaMediana.BackgroundColor = Color.Default;
            pizzaGrande.BackgroundColor = Color.Default;
            pizzaFamiliar.BackgroundColor = Color.BurlyWood;
            t2 = (multi * t) - 30;
        }
        void No(object sender, EventArgs args)
        {
            Refrescar();
            promocion.Text = "";
            Combo.Text = "";
            AhoritaNoJoven.IsVisible = false;
            promocion.Text = "";
            ingre.Text = "";
            opc1.Text = "";
            opc2.Text = "";
            opc3.Text = "";
            opc4.Text = "";
            opc5.Text = "";
            opc6.Text = "";
        }
        private void Pepperoni_Toggled(object sender, ToggledEventArgs e)
        {
            bool isTogged = e.Value;
            if (isTogged)
            {peperoni = true;
            }else{
                peperoni = false;}
        }
        private void Chile_Toggled(object sender, ToggledEventArgs e)
        {
            bool isTogged = e.Value;
            if (isTogged)
            {chile = true; 
            }else{
                chile = false;}
        }
        private void Queso_Toggled(object sender, ToggledEventArgs e)
        {
            bool isTogged = e.Value;
            if (isTogged)
            {queso = true;
            }else{
                queso = false;}
        }
        private void Aceituna_Toggled(object sender, ToggledEventArgs e)
        {
            bool isTogged = e.Value;
            if (isTogged)
            {aceituna = true;
            }else{
                aceituna = false;}
        }
        private void Carne_Toggled(object sender, ToggledEventArgs e)
        {
            bool isTogged = e.Value;
            if (isTogged)
            {carne = true;
            }else{
                carne = false;}
        }
        private void Champiñon_Toggled(object sender, ToggledEventArgs e)
        {
            bool isTogged = e.Value;
            if (isTogged)
            {champo = true;
            }else{
                champo = false;}
        }
        private void Cebolla_Toggled(object sender, ToggledEventArgs e)
        {
            bool isTogged = e.Value;
            if (isTogged)
            {cebolla = true;
            }else{
                cebolla = false;}
        }
        private void PizzaQueso_Tapped(object sender, EventArgs e)
        {
            opc1.Text = "";
            opc2.Text = "";
            opc3.Text = "";
            opc4.Text = "";
            opc5.Text = "";
            opc6.Text = "";
            imgPizzaQueso.Opacity = 1;
            imgPizzaPepperoni.Opacity = .60;
            imgPizzaBandido.Opacity = .60;
            imgPizzaVegana.Opacity = .60;
            imgPizzaSuprema.Opacity = .60;
            Combo.Text = "Total: 258";
            promocion.Text = "¡PROMOCION DESVERGONZADA! - La Original";
            AhoritaNoJoven.IsVisible = (true);
            ingre.Text = "Esta Pizza incluye: ";
            opc1.Text = "     -3 Quesos";
        }
        private void PizzaPepperoni_Tapped(object sender, EventArgs e)
        {
            opc1.Text = "";
            opc2.Text = "";
            opc3.Text = "";
            opc4.Text = "";
            opc5.Text = "";
            opc6.Text = "";
            imgPizzaQueso.Opacity = .60;
            imgPizzaPepperoni.Opacity = 1;
            imgPizzaBandido.Opacity = .60;
            imgPizzaVegana.Opacity = .60;
            imgPizzaSuprema.Opacity = .60;
            Combo.Text = "Total: 250";
            promocion.Text = "¡PROMOCION DESVERGONZADA - La Clásica!";
            AhoritaNoJoven.IsVisible = (true);
            ingre.Text = "Esta Pizza incluye: ";
            opc1.Text = "     -Pepperoni";
        }
        private void PizzaBandido_Tapped(object sender, EventArgs e)
        {
            opc1.Text = "";
            opc2.Text = "";
            opc3.Text = "";
            opc4.Text = "";
            opc5.Text = "";
            opc6.Text = "";
            imgPizzaQueso.Opacity = .60;
            imgPizzaPepperoni.Opacity = .60;
            imgPizzaBandido.Opacity = 1;
            imgPizzaVegana.Opacity = .60;
            imgPizzaSuprema.Opacity = .60;
            Combo.Text = "Total: 358";
            promocion.Text = "¡PROMOCION DESVERGONZADA! - La mas Buscada";
            AhoritaNoJoven.IsVisible = (true);
            ingre.Text = "Esta Pizza incluye: ";
            opc1.Text = "     -Cebolla";
            opc2.Text = "      -Chile";
            opc3.Text = "     -Aceitunas";
            opc4.Text = "     -Carne";
        }
        private void PizzaVegana_Tapped(object sender, EventArgs e)
        {
            opc1.Text = "";
            opc2.Text = "";
            opc3.Text = "";
            opc4.Text = "";
            opc5.Text = "";
            opc6.Text = "";
            imgPizzaQueso.Opacity = .60;
            imgPizzaPepperoni.Opacity = .60;
            imgPizzaBandido.Opacity = .60;
            imgPizzaVegana.Opacity = 1;
            imgPizzaSuprema.Opacity = .60;
            Combo.Text = "Total: 334";
            promocion.Text = "¡PROMOCION DESVERGONZADA! - La Amigable";
            AhoritaNoJoven.IsVisible = (true);
            ingre.Text = "Esta Pizza incluye: ";
            opc1.Text = "     -Cebolla";
            opc2.Text = "      -Chile";
            opc3.Text = "     -Aceitunas";
            opc4.Text = "     -Champiñones";
        }
        private void PizzaSuprema_Tapped(object sender, EventArgs e)
        {
            opc1.Text = "";
            opc2.Text = "";
            opc3.Text = "";
            opc4.Text = "";
            opc5.Text = "";
            opc6.Text = "";
            imgPizzaQueso.Opacity = .60;
            imgPizzaPepperoni.Opacity = .60;
            imgPizzaBandido.Opacity = .60;
            imgPizzaVegana.Opacity = .60;
            imgPizzaSuprema.Opacity = 1;
            /*peperoni = true;
            cebolla = true;
            champo = true;
            aceituna = true;
            carne = true;
            chile = true;
            t2 = 90;
            Evaluar();
            total = b + t2 + c2;*/
            Combo.Text = "Total: 430";
            promocion.Text = "¡PROMOCION DESVERGONZADA! - La Magnífica";
            AhoritaNoJoven.IsVisible=(true);
            ingre.Text = "Esta Pizza incluye: ";
            opc1.Text = "     -Pepperoni";
            opc2.Text = "     -Cebolla";
            opc3.Text = "      -Chile";
            opc4.Text = "     -Aceitunas";
            opc5.Text = "     -Champiñones";
            opc6.Text = "     -Carne";
        }
    }
}